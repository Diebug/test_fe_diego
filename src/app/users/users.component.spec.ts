import {TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

fdescribe('UsersComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      RouterTestingModule
    ],
    declarations: [
      UsersComponent
    ],
  }));

  it('should be created', () => {
    const service: UsersComponent = TestBed.get(UsersComponent);
    expect(service).toBeTruthy();
  });

  it('should load the data into users', () => {
    const service: UsersComponent = TestBed.get(UsersComponent);
    expect(service.users).toBeDefined();
  });

});
