import {
  Injectable,
  Component
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';

const endpoint = 'https://reqres.in/api/users';
@Component({
  selector: 'users-component',
  templateUrl: './users.component.html'
})
@Injectable({
  providedIn: 'root'
})
export class UsersComponent {
  public title = 'prueba';
  public users = [];
  constructor(private http: HttpClient) {
    this.extractData();
  }
  extractData() {
    this.http.get(endpoint).subscribe((data) => {
      this.users.push(data)
    });
  }
  ordenar() {
    this.users[0].data.sort(function (a, b) {
      var nameA = a.first_name.toLowerCase(),
        nameB = b.first_name.toLowerCase();
      if (nameA < nameB)
        return -1;
      if (nameA > nameB)
        return 1;
      return 0;
    });
    console.log(this.users[0].data)
  }
}
