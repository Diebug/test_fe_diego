import { Component } from "@angular/core"
@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html'
})
export class NavBarComponent{
    public navbar: any = [
        {name: "Inicio", path: "/"},
        {name: "Usuarios", path: "/users"}
    ]
}